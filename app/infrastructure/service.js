import ApiClient from './apiClient'

class Service {
  constructor(bus) {
    this.bus = bus
    this.suscriptions()
  }

  suscriptions() {
    this.bus.subscribe('create', this.create.bind(this))
    this.bus.subscribe('retrieve', this.retrieve.bind(this))
  }

  create(payload) {
    const response = ApiClient.postJson('add', payload)
    
    this.bus.publish('created', response)
  }

  retrieve(payload) {
    const response =  ApiClient.getJson('retieves', payload)
    
    this.bus.publish('retrievedList', response)
  }
}

export default Service