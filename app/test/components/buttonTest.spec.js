import Button from '../../src/views/Button'

describe('Button',()=>{
  beforeEach(() => {
    window.customElements.define('my-button', Button)
  })
  
  it('dispatch an event',()=>{
    const button = document.createElement('my-button')
    const mock = jest.fn()
    const event = new Event('click', { bubbles: true })
    document.querySelector('body').appendChild(button)
    document.addEventListener('add', (event) => {mock(event)})

    button.shadowRoot.querySelector('button').dispatchEvent(event)

    expect(mock).toHaveBeenCalled()
  })
})