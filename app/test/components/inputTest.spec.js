import Input from '../../src/views/Input'

describe('Input',()=>{
  beforeEach(() => {
    window.customElements.define('my-input', Input)
  })

  it('dispatch an event',()=>{
    const input = document.createElement('my-input')
    const mock = jest.fn()
    const event = new Event('keyup', { bubbles: true })
    document.querySelector('body').appendChild(input)
    document.addEventListener('inputKeyUp', (event) => {mock(event)})

    input.shadowRoot.querySelector('input').dispatchEvent(event)

    expect(mock).toHaveBeenCalled()
  })
})