import List from '../../src/views/List'

describe('List',()=>{
  beforeEach(() => {
    window.customElements.define('my-list', List)
  })

  it('receives attributes', ()=>{
    const list = document.createElement('my-list')

    list.setAttribute('listitem', 'itemOnList')

    expect(list.shadowRoot.querySelector('ul').innerHTML).toContain('itemOnList')
  })
})