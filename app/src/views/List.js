const listTemplate = document.createElement('template')
listTemplate.innerHTML = `
  <div>
    <ul></ul>
  </div>
`
class List extends HTMLElement {
  constructor() {
    super()
    this.root = this.attachShadow({ mode: 'open' })
    this.root.appendChild(listTemplate.content.cloneNode(true))
    this.list = this.root.querySelector('ul')
    this.emptyList = []
    this.render()
  }

  static get observedAttributes() {
    return ['listitem']
  }

  attributeChangedCallback(name, _oldVal, newVal) {
    this[name] = newVal
    this.render()
  } 

  render() {
    let added = this.getAttribute('listitem')
    this.emptyList.push(added)
    let listLi = document.createElement('li')
    listLi.innerHTML = this.getAttribute('listitem')
    this.list.appendChild(listLi)
  }
}

export default List