const inputTemplate = document.createElement('template')
inputTemplate.innerHTML = `
  <div>
    <input></input>
  </div>
`
class Input extends HTMLElement {
  constructor() {
    super()
    this.root = this.attachShadow({ mode: 'open' })
    this.root.appendChild(inputTemplate.content.cloneNode(true))
    this.input = this.root.querySelector('input')
  }

  connectedCallback() {
    this.input.addEventListener('keyup', this.send.bind(this))
  }

  send(event) {
    this.dispatchEvent(
      new CustomEvent('inputKeyUp', 
      { bubbles: true, 
        detail: event.target.value
      })
    )
  }
}

export default Input