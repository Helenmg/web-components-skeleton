const buttonTemplate = document.createElement('template')
buttonTemplate.innerHTML = `
  <div>
    <button></button>
  </div>
`
class Button extends HTMLElement {
  constructor() {
    super()
    this.root = this.attachShadow({ mode: 'open' })
    this.root.appendChild(buttonTemplate.content.cloneNode(true))
    this.button = this.root.querySelector('button')
  }

  connectedCallback() {
    this.button.addEventListener('click', this.add.bind(this))
  }

  add(event) {
    this.dispatchEvent(
      new CustomEvent('add', 
      { bubbles: true, 
        detail: event
      })
    )
  }
}

export default Button