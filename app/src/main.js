import Input from '../src/views/Input'
import Button from '../src/views/Button'
import List from '../src/views/List'

window.customElements.define('my-input', Input)
window.customElements.define('my-button', Button)
window.customElements.define('my-list', List)

import Component from './Component'
import Service from '../infrastructure/service'
import Bus from '../infrastructure/bus'

const bus = new Bus()

new Component(bus)
new Service(bus)


