class Component {
  constructor(bus) {
    this.bus = bus
    this.subscriptions()
    this.watchActions()
    this.inputList = []
    this.componentList = document.querySelector('my-list')
  }
  
  subscriptions() {
    this.bus.subscribe('created', this.listItems.bind(this))
    this.bus.subscribe('retrievedList', this.listItems.bind(this))
  }

  watchActions() {
    document.addEventListener('inputKeyUp', this.inputEvent.bind(this))
    document.addEventListener('add', this.addToList.bind(this))
  }

  inputEvent(event){
    this.inputList = event.detail
  }

  create(payload) {
    this.bus.publish('create', payload)
  }

  listItems(payload) {
    this.componentList.setAttribute('lisitem', payload)
  }

  addToList() {
    this.bus.publish('retrieve')
    
    this.componentList.setAttribute('listitem', this.inputList)
  }
}

export default Component