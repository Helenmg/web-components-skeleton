 This application is a skeleton for developers:

<img src="./app/img/portal.png" alt="portal"
	title="portal" />

  ## Table of Contents

  - [System requeriments](#Systemrequeriments)
  - [How to run the application](#Howtoruntheapplication)
  - [How to run test](#Howtoruntest)


  ## System requeriments
  - Chrome last version  79.0.3945.88
  - Firefox last version  73.0.1
  - Docker-compose version 1.24.1

  ## How to run the application

  Console use command:

   `docker-compose up --build`

  APP:
  
   `localhost:8080`

  ## How to run test 

  `docker-compose exec app npm run test`

  `docker-compose run --rm e2e`.